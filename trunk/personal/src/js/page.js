var gt = {
    initPageResponse: function () {
        var page = new pageResponse({
            selectors: '.section__content', //模块的类名，使用class来控制页面上的模块(1个或多个)
            mode: 'cover', // auto || contain || cover
            width: '640', //输入页面的宽度，只支持输入数值，默认宽度为320px
            height: '1020'      //输入页面的高度，只支持输入数值，默认高度为504px
        });
    },
    bindEvents: function () {
        $(window).resize(function () {
            gt.initPageResponse();
        });
        // $('.main-env').bind('tap', function () {
        //     $('.letter-page').addClass('active');
        //     setTimeout(function () {
        //         $('.letter-page').hide();
        //     }, 600);
        // });
    },


    initHighcharts: function () {
        var bodyHeight = $(document.body).height();
        var top = 60, size = '80%';
        var chart = $('.section6 .sec3__content-box .chart1');
        if (bodyHeight <= 480) {
            top = 10;
            size = '30%';
        }
        chart.highcharts({

            credits:{
                enabled:false
            },
            chart: {
                polar: true,
                type:'area'
            },
            legend:{
                enabled:false
            },
            exporting: {
                enabled: false
            },
            title: {
                text: null
            },
            tooltip: {       //提示框
                enabled:false
            },
            pane: {
                startAngle: 0,
                endAngle: 360,
                size: size
            },
            xAxis: {
                tickInterval: 72,
                lineColor:'#ffc0b4',
                gridLineWidth: 0,      //设置X轴线宽度为0
                // gridLineLength:2,
                labels: {
                    formatter: function () {
                        if(this.value===0)
                        {
                            return '收益能力';
                        }
                        else if(this.value===72)
                        {
                            return '选股能力';
                        }
                        else if(this.value===144)
                        {
                            return '仓位控制';
                        }
                        else if(this.value===216)
                        {
                            return '盘面感知';
                        }
                        else if(this.value===288)
                        {
                            return '风险控制';
                        }
                    },
                    style:{
                        color:'#999999',
                        fontSize:'20px'
                    }
                }
            },
            yAxis: {
                labels:
                {
                    enabled:false
                },
                tickInterval:5,           //轴的递增频率
                //tickAmount: 4,
                gridLineColor:'#ffc0b4',      //Y轴线颜色
                gridLineWidth: 2,
                plotBands: [{ // visualize the weekend
                    from: 0,
                    to: 20,
                    color: '#fffaf9'
                }],
                max:20
            },
            plotOptions: {
                area:{
                    marker: {
                        enabled: false
                    },
                    lineWidth:0
                },
                series: {
                    pointStart: 0,
                    pointInterval: 72,
                    enabled:false,
                    states:{
                        hover:{
                            enabled:false
                        }
                    }
                },
                column: {
                    pointPadding: 0,
                    groupPadding: 0
                }
            },
            series: [{
                name: '2015',
                data: [10, 8, 5, 11, 5],
                fillColor: {
                    linearGradient: [20, 150, 50, 300],
                    stops: [
                        [0, '#e53539'],
                        [1, '#fc6007']
                    ]
                }
            }, {
                name: '2016',
                data: [8, 7, 15, 7,10],
                fillColor: {
                    linearGradient: [20, 150, 50, 300],
                    stops: [
                        [0, '#ffe400'],
                        [1, '#ff8200']
                    ]
                }
            },{
                name: '2017',
                type:'line',
                marker:{
                    enabled: true,
                    radius: 5,
                    symbol:'circle'
                },
                lineColor: "none",
                fillColor:'none',
                color:'#ff6840',
                data: [20, 20, 20, 20, 20]
            }]

        });
        /**
         *加载收益率图表
         *
         * @param type    盈利0， 亏损1
         * @param data    图表数据
         */
        function initIncomeHighcharts(type, data) {
            var chartId;
            var fontColor;
            var pointColor;
            //盈亏情况下不同文案
            var txt1 = '10月能量爆棚，hold住好状态！';
            var txt2 = '7月能量爆棚，hold住好状态！';
            var txt3 = '7月能量爆棚，这才是我！';
            var txt4 = '8月大意了，关注公众号吧，专业课程来助力！';
            var txt5 = '8月大意了，体验小君课程吧，为您助力！';
            var txt6 = '公众号：国泰君安微理财（gtjawlc）';

            if (type == 0) {
                //收益为正
                chartId = 'income-chart';
                fontColor = '#f3620a'; //坐标轴刻度值颜色
                pointColor = '#ff7200'; //折线点颜色
                $('.section__content.gain').show(); //显示和隐藏盈利页
                $('.section__content.deficit').hide();

                //if(gt.UtilsIsWeiXin){//微理财环境
                //	$('.gain .section__content-bottom-txt').children('p').eq(0).text(txt2);
                //}else{//易阳指环境
                //	$('.gain .section__content-bottom-txt').children('p').eq(0).text(txt1);
                //}
            } else {
                //收益为负
                chartId = 'deficit-chart';
                $('.section__content.deficit').show();
                $('.section__content.gain').hide();
                // fontColor = '#2ca9e1';
                // pointColor = '#50abf7';
                fontColor = '#f3620a'; //坐标轴刻度值颜色
                pointColor = '#ff7200'; //折线点颜色

                if (gt.UtilsIsWeiXin) {//微信环境
                    //if(){//有赚
                    //	$('.deficit .section__content-bottom-txt').children('p').eq(0).text(txt3);
                    //}else{//全亏
                    //	$('.deficit .section__content-bottom-txt').children('p').eq(0).text(txt5);
                    //}
                } else {//易阳指环境
                    //if(){//有赚
                    //	$('.deficit .section__content-bottom-txt').children('p').eq(0).text(txt1);
                    //}else{//全亏
                    //	$('.gain .section__content-bottom-txt').children('p').eq(0).text(txt4);
                    //}
                }
            }

            // if (gt.UtilsIsWeiXin) {//微理财环境
            //     $('.section__content-bottom-txt').children('p').eq(1).text('');
            // } else {//易阳指环境
            //     $('.section__content-bottom-txt').children('p').eq(1).text(txt6);
            // }

            $('.section1 #' + chartId).highcharts({
                chart: {
                    backgroundColor: 'rgba(0,0,0,0)'
                },
                credits: {
                    enabled: false
                },
                tooltip: {
                    enabled: false
                },
                exporting: {
                    enabled: false
                },
                title: {
                    text: '各月战斗力',
                    style: {
                        color: "#ffffff",
                        fontSize: 30,
                        fontFamily: 'Helvetica Neue'
                    },
                    margin: 35,
                    y: 50
                },
                xAxis: [{
                    gridLineColor: '#f98a7c',
                    gridLineWidth: 1,
                    lineColor: '#ffede1',
                    tickLength: 0,
                    lineWidth: 1,
                    tickPixelInterval: 120,
                    categories: ['4月', '5月', '6月', '7月', '8月', '9月'],
                    labels: {
                        style: {
                            fontSize: '28px',
                            color: '#ffede1'
                        }
                    }
                }, {
                    lineColor: '#ffede1',
                    lineWidth: 1,
                    lineDashStyle: 'Dash',
                    opposite: true
                }],
                yAxis: {
                    title: {
                        text: null
                    },
                    gridLineColor: '#fb7e6d',
                    gridLineWidth: 0,
                    lineWidth: 1,
                    lineColor: '#fb7e6d',
                    tickInterval: 3000,
                    max: 6000,
                    min: -3000,
                    labels: {
                        align: 'left',
                        x: 10,
                        style: {
                            fontSize: '24px',
                            color: '#ffede1'
                        },
                        formatter: function () {
                            if (this.value < 0) {
                                return -3000;
                            } else if (this.value == 0) {
                                return 0;
                            } else if (this.value >= 6000) {
                                return 6000;
                            }
                        }
                    }
                },
                legend: {
                    enabled: false,
                    itemStyle: {
                        color: '#444444',
                        fontSize: '1.1rem',
                        fontWeight: 'normal'
                    }
                },
                series: [{
                    type: 'spline',
                    name: '全民',
                    lineWidth: 8,
                    color: '#fff8f7',
                    data: [500, 1500, -2000, 1500, -1000, 5000],
                    marker: {
                        fillColor: pointColor,
                        lineWidth: 4,
                        radius: 12,  //曲线点半径，默认是4
                        symbol: 'circle',
                        lineColor: '#ffffff'
                    }
                }]
            });
        }

        initIncomeHighcharts(0);

        /*收益率图表*/
        $('.section2 #chart').highcharts({
            chart: {
                type: 'areaspline',
                height:180
            },
            title: {
                text: null,     //标题
                style:{
                    fontSize:'24px',
                    color:'#5e5e5e'
                },
                y:50
            },
            exporting: {
                enabled: false
            },
            legend: {
                enabled:false
            },
            xAxis: {
                tickWidth:0,
                tickInterval:2,
                min:0,
                lineColor:'#ffb362',
                lineWidth:3,
                labels:{
                    style:{
                        color:'#a9a9a9',
                        fontSize:'18px'
                    },
                    x:-20,
                    formatter: function () {
                        if(this.value===0)
                        {
                            return '';
                        }
                        else if(this.value===2)
                        {
                            return '一季度';
                        }
                        else if(this.value===4)
                        {
                            return '二季度';
                        }
                        else if(this.value===6)
                        {
                            return '三季度';
                        }
                        else if(this.value===8)
                        {
                            return '四季度';
                        }
                    }
                },
                startOnTick: false,
                endOnTick:false,
                maxPadding:0
            },
            yAxis: {
                gridLineWidth:0,
                labels: {
                    enabled: false
                },
                title:null,
                // showFirstLabel:false,
                min:0,
                maxPadding:0
            },
            tooltip: {       //提示框
                enabled:false
            },
            credits: {
                enabled: false
            },
            plotOptions: {
                areaspline: {
                    fillOpacity: 0.2
                },
                states:{
                    hover:{
                        enabled: false
                    }
                }
            },
            series: [{
                name: 'a',
                data: [0, 7,6, 12, 6,9,3,7,5,0],
                color:'#ffba25',
                marker:
                {
                    enabled:false
                }
            }, {
                name: 'b',
                data: [{
                    y:0,
                    marker:{enabled:false}
                },5,8, 4, 13,6,9,3,10,{
                    y:0,
                    marker:{enabled:false}
                }],
                color:'#ff6c4c',
                marker: {
                    symbol:'circle',
                    fillColor: '#fff6d2',
                    lineWidth: 2,
                    lineColor: null, // inherit from series
                    states:{
                        hover:{
                            enabled:false
                        }
                    }
                }
            }]
        });

        /*仓位控制图表*/
        //$('.section7 #chart2')
        $('.section7 #chart2').highcharts({
            chart: {
                type: 'areaspline',
                backgroundColor: '#f7f7f7'
                // borderRadius:10
            },
            title: {
                text: null
            },
            legend: {
                enabled: false
            },
            exporting: {
                enabled: false
            },
            xAxis: [{
                tickWidth:0,
                gridLineWidth:1,
                opposite : true,
                labels:{
                    formatter:function(){
                        if(this.value===1)
                        {
                            return '2月';
                        }
                        else if(this.value===2)
                        {
                            return '4月';
                        }
                        else if(this.value===3)
                        {
                            return '6月';
                        }
                        else if(this.value===4)
                        {
                            return '8月';
                        }
                        else if(this.value===5)
                        {
                            return '10月';
                        }
                        else if(this.value===6)
                        {
                            return '12月';
                        }
                    },
                    style: {
                        color: '#9a9a9a',
                        fontSize:'16px'
                    },
                    align:'right',
                    x:-20,
                    y:20
                }
            }],
            yAxis: [{
                title: null,
                labels:{
                    format:'{value}%',
                    style: {
                        color: '#9a9a9a',
                        fontSize:'14px'
                    }
                },
                showFirstLabel: false,
                showLastLabel: false,
                tickAmount: 4,
                //tickInterval:0.5,
                lineWidth:1,
                gridLineWidth:0,
                tickLength:5,
                tickWidth:1,
                tickPosition: 'inside',
                offset:-5
            },{
                title: null,
                labels: {
                    format: '{value}',
                    style: {
                        color: '#9a9a9a',
                        fontSize:'14px'
                    }
                },
                showFirstLabel: false,
                showLastLabel: false,
                tickAmount: 4,
                opposite: true,
                lineWidth:1,
                gridLineWidth:0,
                tickLength:5,
                tickWidth:1,
                tickPosition: 'on',
                offset:-5,
                min:3400,
                max:4000,
                startOnTick:false,
                //tickPixelInterval: 500
                // minTick:500
                // tickInterval:3000
            }],
            tooltip: {
                enabled:false
            },
            credits: {
                enabled: false
            },
            plotOptions: {
                areaspline: {
                    fillOpacity: 0.3
                },
                states:{
                    hover:{
                        enabled: false
                    }
                }
            },
            series: [ {
                name: '泸深300指数',
                data: [3800, 3900, 3850, 3980, 3850, 3920,3706],
                type:'spline',
                yAxis:1,
                marker:
                {
                    enabled:false,
                    states:{
                        hover:{
                            enabled:false
                        }
                    }
                },
                lineWidth:6,
                color:'#ece4e4'
            },{
                name: '股票仓位%',
                data: [0.4, 0.2, 0.6, 0.35, 0.5, 0.4,0.3],
                type:'areaspline',
                // yAxis:1,
                marker:
                {
                    enabled:false
                },
                lineWidth:0,    //边框宽度
                fillColor:{
                    linearGradient: [100, 200, 100, 300],   //(x1,y1,x2,y2)开始渐变的x坐标，y坐标，结束渐变的x坐标，y坐标
                    stops: [
                        [0, 'rgba(255, 67, 30, 0.7)'],
                        [1, 'rgba(244, 58, 25, 0.85)']
                    ]
                }
            },{
                name: '达人仓位',
                data: [0.15, 0.3, 0.2, 0.5, 0.3, 0.2,0.28],
                type:'areaspline',
                //yAxis:1,
                marker:
                {
                    enabled:false,
                    states:{
                        hover:{
                            enabled:false
                        }
                    }
                },
                lineWidth:0 ,   //边框宽度
                fillColor:{
                    linearGradient: [100, 200, 100, 300],   //(x1,y1,x2,y2)开始渐变的x坐标，y坐标，结束渐变的x坐标，y坐标
                    stops: [
                        [0, 'rgba(255, 237, 20, 0.79)'],
                        [1, 'rgba(252, 190, 21, 0.88)']
                    ]
                }
            }]
        });



    },
    initPageSlider: function () {
        new PageSlider({
            pages: Zepto('#fullpage .section'),
            gestureFollowing: true,
            direction: 'vertical', //可选，vertical 或 v 为上下滑动，horizontal 或 h 为左右滑动，默认为 vertical
            currentClass: 'current', //可选, 当前屏的class (方便实现内容的进场动画)，默认值为 'current'
            hasDot: 'false', //可选，生成标识点结构，样式自己控制
            preventDefault: true, //可选，是否阻止默认行为
            rememberLastVisited: true, //可选，记住上一次访问结束后的索引值，可用于实现页面返回后是否回到上次访问的页面
            animationPlayOnce: false, //可选，切换页面时，动画只执行一次
            dev: 0, //可选，开发模式，传入具体页面索引值,
            onchange: function () {
                if (this.index === 2) {
                    gt.initPage3();
                }
                // 隐藏最后一屏滑动icon
                if ((this.index) == 9) {
                    $('.arrow-slidedown-nav').hide();
                } else {
                    $('.arrow-slidedown-nav').show();
                }
            }
        });
    },
    initPage3: function () {
        var star = 1;
        var txt = '点石成金';
        $('.section__content-star-img').attr('src', '../img/4_upper_star' + star + '@2x.png');
        $('.section__content-star .section__content-star-text').text(txt);
    },
    // defer images load after load first image
    initDeferLoadPageImage: function () {
        var imgDefer = $("img").not('#network-first-load-img');
        imgDefer.each(function () {
            if ($(this).data('src')) {
                $(this).attr('src', $(this).data('src'));
            }
        });
    },
    initOverlay: function () {
        //弹出层 参数配置
        var options = {
            closeBtn: false,
            scrolling: "visible",
            transitionIn: 'elastic',
            transitionOut: 'elastic',
            padding: '0'
        };

        //触发活动规则 popup
        $("#section11-active-rule-btn").fancybox(options);

        //触发关注公众号 popup
        $("#section11-get-course").fancybox(options);

        //触发 分享赚话费成功 or 话费发完了 popup
        $("#section11-share-earn-money").fancybox(options);

        //触发关注公众号 popup
        $("#share-earn-money-success-get-reward").fancybox(options);

        //触发关注公众号 popup
        $("#share-phone-over-get-course").fancybox(options);

        //popup 关闭时间出发
        $(".popup .popup__close").click(function () {
            $.fancybox.close();
        });

        if (gt.UtilsIsWeiXin()) {//微理财显示、易阳指隐藏
            $(".section9 .section__content-bottom-wlc").show();
            $(".section9 .section__content-bottom-yyz").hide();
        } else {//易阳指显示、微理财隐藏
            $(".section9 .section__content-bottom-wlc").hide();
            $(".section9 .section__content-bottom-yyz").show();
        }
    },
    bindWeixinShare: function () {
        $("#section__content-bottom-wlc-share-friend").click(function () {
            $("#section__content-bottom-wlc-popup").show();
        });
        $("#section__content-bottom-wlc-popup").click(function () {
            $(this).hide();
        });
    },
    // window.onload
    bindWindowOnload: function () {
        window.onload = function () {
            gt.initDeferLoadPageImage();
            gt.initHighcharts();
        }
    },
    // 检测当前环境是否为微信
    UtilsIsWeiXin: function () {
        var ua = window.navigator.userAgent.toLowerCase();
        if (ua.match(/MicroMessenger/i) == 'micromessenger') {
            return true;
        } else {
            return false;
        }
    },
    initLetterBox: function () {
        var letterPage = $('.letter-page');
        var wrapper = $('#wrapper');
        wrapper.css({
            'transform': 'translate3d(0, 100%, 0)'
        });

        letterPage.find('.letter-box').bind('touchstart', this.onTouchLetterBtn);
    },

    onTouchLetterBtn: function () {
        var letterPage = $('.letter-page');
        var wrapper = $('#wrapper');
        letterPage.find('.evn-people').hide();
        letterPage.find('.gai .img-gai').addClass('turn');
        letterPage.find('.logo .cover, .letter-page .logo .light').hide();
        letterPage.find('.logo .light').removeClass('move');
        letterPage.find('.logo .normal').show();
        letterPage.find('.letter-box').addClass('open');

        letterPage.find('.gai').animate({'z-index': '0'}, 1000, function () {
            wrapper.show();
            setTimeout(function () {
                wrapper.css({
                    'transform': 'translate3d(0, 0, 0)'
                });
                // <- 6/28
            });

            letterPage.animate({opacity: '0'}, 300, function () {
                letterPage.hide();
            });

        });

        //setTimeout(function () {
        //
        //
        //}, 1000);
    },

    // 主入口
    init: function () {
        this.bindWindowOnload();
        // this.onTouchLetterBtn();
        this.initLetterBox();
        this.initPageResponse();
        this.bindEvents();
        this.initPageSlider();
        this.initOverlay();
        this.bindWeixinShare();

    }

};

$(function () {
    gt.init();
});
