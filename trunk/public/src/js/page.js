var gt = {
    initPageResponse: function () {
        var page = new pageResponse({
            selectors: '.section__content', //模块的类名，使用class来控制页面上的模块(1个或多个)
            mode: 'cover', // auto || contain || cover
            width: '640', //输入页面的宽度，只支持输入数值，默认宽度为320px
            height: '1020'      //输入页面的高度，只支持输入数值，默认高度为504px
        });
    },
    bindPageResponse: function () {
        $(window).resize(function () {
            gt.initPageResponse();
        });
    },
    initHighcharts: function () {
        $('.section3 .highchart').highcharts({
            chart: {
                plotBackgroundColor: '#e3f4ff'
            },
            colors: ['#ff7200', '#52c3f1', '#0080cc'],
            credits: {
                enabled: false
            },
            tooltip: {
                enabled: false
            },
            exporting: {
                enabled: false
            },
            title: {
                text: null
            },
            xAxis: {
                gridLineColor: '#b8ebff',
                gridLineWidth: 1,
                lineColor: '#b8ebff',
                tickLength: 0,
                lineWidth: 2,
                categories: ['', '', '7月', '', '8月', '', '9月', '', ''],
                labels: {
                    style: {
                        fontSize: '22px'
                    }
                }
            },
            yAxis: {
                title: {
                    text: null
                },
                gridLineColor: '#b8ebff',
                gridLineWidth: 1,
                lineWidth: 2,
                lineColor: '#b8ebff',
                tickInterval: 1,
                labels: {
                    enabled: false
                }
            },
            legend: {
                enabled: false,
                itemStyle: {
                    color: '#444444',
                    fontSize: '1.1rem',
                    fontWeight: 'normal'
                }
            },
            series: [{
                name: '全民',
                lineWidth: 4,
                data: [7, 6, 9, 4, 3, 1, 2, 5]
            }, {
                name: '牛人',
                lineWidth: 4,
                data: [2, 3, 5, 4, 5, 7, 6, 8]
            }, {
                name: '大盘',
                lineWidth: 4,
                data: [3, 4, 3, 8, 3, 2, 1, 6]
            }]
        });
    },
    // defer images load after load first image
    initDeferLoadPageImage: function () {
        var imgDefer = $('img').not('#network-first-load-img');
        imgDefer.each(function () {
            if ($(this).data('src')) {
                $(this).attr('src', $(this).data('src'));
            }
        });
    },
    // defer highcharts.js
    initDeferLoadHighCharts: function () {
        var element = document.createElement('script');
        element.src = '../media/js/vendor/highcharts.js';
        document.body.appendChild(element);
    },
    initPageSlider: function () {
        new PageSlider({
            pages: Zepto('#fullpage .section'),
            gestureFollowing: true,
            direction: 'vertical', //可选，vertical 或 v 为上下滑动，horizontal 或 h 为左右滑动，默认为 vertical
            currentClass: 'current', //可选, 当前屏的class (方便实现内容的进场动画)，默认值为 'current'
            hasDot: false, //可选，生成标识点结构，样式自己控制
            preventDefault: true, //可选，是否阻止默认行为
            rememberLastVisited: true, //可选，记住上一次访问结束后的索引值，可用于实现页面返回后是否回到上次访问的页面
            animationPlayOnce: false, //可选，切换页面时，动画只执行一次
            dev: 0, //可选，开发模式，传入具体页面索引值
            onchange: function () {
                if ((this.index) == 2) {
                    gt.initHighcharts();
                }
                // 隐藏最后一屏滑动icon
                if ((this.index) == 8) {
                    $('.arrow-slidedown-nav').hide();
                    gt.initSection9();
                } else {
                    $('.arrow-slidedown-nav').show();
                }
            }
        });
    },
    bindSectionQuestion: function () {
        $('.section .section__content-bottom li').bind('click', function () {
            var thisId = $(this).children().data('id');
            $(this).addClass('actived').siblings('li').removeClass('actived');
            $('.section-money-source-from-type').val(thisId);
        });
    },
    initSection9: function () {


    },
    // 第九屏小屏自动裁剪
    bindSection9: function () {
        var section_9_height = $(window).height() + 80;
        $('.section.section9 .section__content-top').height(section_9_height + 'px');
    },
    bindWeixinShare: function () {
        var star = 3;   // 取值请确保为1|2|3中的一个
        var isYYZ = gt.UtilsIsWeiXin() ? 0 : 1;
        // var isYYZ = 0;
        var imgName = 'banner-section9__star' + star + '-' + isYYZ + '.jpg';
        $('.section9 .section__content-top').css('background-image', 'url("../img/' + imgName + '")');

        if (!isYYZ) {//微信环境
            // $('#section__content-bottom-share-friend_wlc').show();
            // $('#section__content-bottom-share-friend_yyz').hide();
            $('#section__content-bottom-download-yyz').hide();
            $('.section9 .text').hide();
        } else {//易阳指环境
            $('#section__content-bottom-share-friend_wlc').hide();
            // $('#section__content-bottom-share-friend_yyz').show();
            $('.section9 .section__content-bottom-table').hide();
            $('#section__content-bottom-download-yyz').css('display', 'inline-block');
        }

        $('#section__content-bottom-share-friend_wlc').bind('touchstart', function () {
            $('#section__content-bottom-wlc-popup').show();
        });
        $('#section__content-bottom-wlc-popup').bind('touchstart', function () {
            $(this).hide();
        });
    },
    // window.onload
    bindWindowOnload: function () {
        window.onload = function () {
            gt.initDeferLoadPageImage();
            gt.initDeferLoadHighCharts();
        }
    },
    // 检测当前环境是否为微信
    UtilsIsWeiXin: function () {
        var ua = window.navigator.userAgent.toLowerCase();
        if (ua.match(/MicroMessenger/i) == 'micromessenger') {
            return true;
        } else {
            return false;
        }
    },
    // 主入口
    init: function () {
        this.initPageResponse();
        this.bindPageResponse();
        this.initPageSlider();
        this.bindWeixinShare();
        this.bindWindowOnload();
        this.bindSectionQuestion();
        this.bindSection9();

    }

};

$(function () {
    gt.init();
});

